<!DOCTYPE html>
<html>

<body>
    <?php

    echo "Hola Mundo";
    echo "<br>";
    $color = "azul";
    echo "Mi coche es " . $color . "<br>";
    //echo "Mi camisa es " . $COLOR . "<br>"; //Ok, como vemos las variables son sensibles a mayúsculas
    //echo "Mi cuaderno es " . $coLOR . "<br>";
    $dia1 = 15;
    $dia2 = 30;
    echo "El valor de la primera variable es ";
    echo $dia1;
    echo "<br>";
    echo "El valor de la segunda variable es ";
    echo $dia2;
    echo "<br>";
    $dia = date("d");
    if ($dia <= 15) {
        echo "Sitio activo";
    } else {
        echo "Sitio fuera de servicio";
    }
    echo "<br>";

    $dia = date("j");
    $mes = date("n");
    $year = date("Y");
    echo "Hoy es " . $dia . " del mes " .
        $mes . " de " . $year

    ?>
</body>

</html>