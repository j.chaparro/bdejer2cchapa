<?php
$bus = $_GET["dep"];
$format = "html";
if (isset($_GET["format"])) {
    $format = $_GET["format"];
}


$conn = new PDO("mysql:host=localhost;dbname=facultad;charset=utf8", "root", "");

$q = $conn->query("SELECT * FROM DEPARTAMENTO WHERE nombreCompleto LIKE '%$bus%'");

if ($q != null) {

    if ($format == "json") {
        header("content-type:application/json");
        $data = $q->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    } else if ($format == "html") {
        header('Content-Type: text/html; charset=utf-8');
        echo "<ul>\n";
        foreach ($q as $dep) {
            print("\t<li>" . $dep["nombreCompleto"] . "</li>\n");
        }
        echo "</ul>\n";
    } else {
        echo "Formato no válido";
    }
}
$conn = null;
