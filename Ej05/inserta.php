<?php
$id = $_POST["idD"];
$inic = $_POST["inicD"];
$nombre = $_POST["nombreD"];

$conn = new PDO("mysql:host=localhost;dbname=facultad;charset=utf8", "root", "");

$sql = "INSERT INTO DEPARTAMENTO (idDepartamento, nombre, nombreCompleto) VALUES (:a, :b, :c)";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':a', $id, PDO::PARAM_INT);
$stmt->bindParam(':b', $inic, PDO::PARAM_STR);
$stmt->bindParam(':c', $nombre, PDO::PARAM_STR);


$stmt->execute();
$conn = null;
