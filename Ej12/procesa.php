<?php
header('Content-Type: text/html; charset=UTF-8');

$idD = $_REQUEST['idD'];
$nD = $_REQUEST['nD'];
$nCD = $_REQUEST['nCD'];
$accion = $_REQUEST['accion'];

$db = 'mysql:host=localhost;dbname=facultad;charset=utf8';
$user = 'root';
$pass = '';

try {
    $conn = new PDO($db, $user, $pass);
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    print "No se pudo conectar a la base de datos: " .
            utf8_encode($e->getMessage()) . "<br/>";
    $conn = null;
    die();
}

try {

    if ($accion != "ve") {
        if ($accion == "inserta") {
            if (existeId($conn, $idD)) {
                print "El ID introducido ya se encuentra en la base de datos, por lo que no se puede insertar<br/>";
                $conn = null;
                die();
            }
            $sql = "INSERT INTO DEPARTAMENTO (idDepartamento, nombre, nombreCompleto) VALUES (:idD, :nD, :nCD)";
        } else {
            if (!existeId($conn, $idD)) {
                print "El ID introducido no se encuentra en la base de datos, por lo que no se puede continuar";
                $conn = null;
                die();
            }

            if ($accion == "modifica") {
                $sql = "UPDATE DEPARTAMENTO SET nombre = :nD, nombreCompleto = :nCD WHERE idDepartamento = :idD";
            } else if ($accion == "elimina")
                $sql = "DELETE FROM DEPARTAMENTO WHERE idDepartamento = :idD";
            else if ($accion == "muestra") {
                $sql = "SELECT * FROM DEPARTAMENTO where idDepartamento = :idD";
            }
        }
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':idD', $idD, PDO::PARAM_INT);

        if ($accion == "inserta" || $accion == "modifica") {
            $stmt->bindParam(':nD', $nD, PDO::PARAM_STR);
            $stmt->bindParam(':nCD', $nCD, PDO::PARAM_STR);
        }
        $success = $stmt->execute();
?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php
        if ($accion == "inserta")
            echo "\nEl departamento " . $nD . ", con id " . $idD . " y nombre completo '" . $nCD .
                "' se ha insertado correctamente en la base de datos";
        else if ($accion == "modifica")
            echo "\nEl departamento con id " . $idD . " se ha actualizado correctamente al nombre '" . $nD . "' y nombre completo '" . $nCD .
                "'";
        else if ($accion == "elimina")
            echo "\nEl departamento con id " . $idD . " se ha eliminado correctamente. ";
        else if ($success) {
            while ($fila = $stmt->fetch()) {
                echo "\nEl departamento de id " . $idD .
                    " tiene como nombre '" . $fila['nombre'] .
                    "' y como nombre completo '" . $fila['nombreCompleto'] . "'";
            }
        }
    ?>
</div>

<?php
        $conn = null;
    }
} catch (PDOException $e) {
?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <?php
    print "No se pudo completar la acción " . $accion .
            utf8_encode($e->getMessage()) . "<br/>";
    ?>
</div>
<?php
    $conn = null;
    die();
}
?>

<?php
if ($accion == "ve"){
?>

<table class="table table-striped table-hover display" id="tablaD">
    <caption>Departamentos</caption>
    <thead>
    <tr>
        <th>ID</th>
        <th>Siglas</th>
        <th>Nombre completo</th>
        <th>E-mail</th>
    </tr>
    </thead>
    <tbody>
    <?php

    try {
        foreach ($conn->query('SELECT * from DEPARTAMENTO') as $fila) {
            echo '<tr><td>', $fila['idDepartamento'], '</td>', '<td>',
            $fila['nombre'], '</td>', '<td>',
            $fila['nombreCompleto'], '</td>', '<td>', '<a href="mailto:', $fila['email'], '">',
            $fila['email'], '</a>', '</td>', '</tr>';
        }
    } catch (PDOException $e){
        print "¡Error!: " . utf8_encode($e->getMessage()) . "<br/>";
        die();
    }
    $conn = null;

    ?>
</tbody>
</table>

<?php
}
?>

<?php
function existeId($conn, $id){
    $sql = "SELECT * FROM DEPARTAMENTO where idDepartamento = :idD";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':idD', $id, PDO::PARAM_INT);
    $stmt->execute();
    return ($stmt->rowCount()>0);
}
?>
